package com.sda3.amen.concurrency.manufacture;

import java.util.Observable;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/*
 * Nazwa worker - od pracownika. to jest asynchroniczny /pracowniczy wątek.
 * 
 * IMPLEMENTUJEMY Observable (mamy dzięki temu metody setChanged i notifyObservers którymi
 * możemy powiadomić observerów o zmianie) , oraz Runnable - niezbędne aby obiekt mógł być
 * wykonywalny przez wątek.
 */
public class FactoryWorker extends Observable implements Runnable{
	
	private long workTime;
	private String name;
	
	/**
	 * Przekazujemy:
	 * @param workTime - czas uspienia workera
	 * @param name - nazwę workera.
	 */
	public FactoryWorker(long workTime, String name) {
		super();
		this.workTime = workTime;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Metoda uruchamiana po wystartowaniu wątku.
	 */
	@Override
	public void run() {
		System.out.println("Wątek "+name+" zaczyna pracę.");
		try{
			Thread.sleep(workTime); // usypiamy
		}catch (InterruptedException e) {
			System.err.println("Error, wątek przerwany");
		}
		
		// tutaj wywołujemy zmianę
		setChanged(); // zmieniamy flagę changed
		notifyObservers(new Random().nextInt(10)); // informujemy observerów o losowej liczbie.
	}
}
