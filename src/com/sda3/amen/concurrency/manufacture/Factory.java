package com.sda3.amen.concurrency.manufacture;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class Factory implements Observer{
//	private List<FactoryWorker> workers;
	
	// dodajemy zadanie - zadaniem jest Worker
	// zadaniem workera jest wykonanie jakiejś pracy, która jest czasochłonna i która
	// powinna się wydarzyć gdzieś w tle. Factory worker implementuje runnable, dlatego może być
	// wywołany przez Thread (przekazujemy jako parametr.
	public void addTask(long timeout, String name){
		FactoryWorker worker = new FactoryWorker(timeout, name);
		worker.addObserver(this);
		
		// tutaj również wywołanie threada i start.
		// pod tym uruchomienie threada z anonimową instancją runnable.
		new Thread(worker).start();
//		new Thread(new Runnable(){
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//				
//			}
//		}).start();
	}
	
	/**
	 * Metoda którą musimy nadpisać z interfejsu Observer. 
	 * Po tym jak dowolny observable (którego obserwujemy!) 
	 * wywoła metody setChanged() i notifyObservers zostaniemy o tym
	 * powiadomieni, ponieważ wykona się ta metoda update.
	 * Pierwszym parametrem metody jest Observable - obiekt który wywołał metodę
	 * drugim parametrem jest argument - informacja o której powiadamiamy (to argument metody:
	 * notifyObservers(Object arg) )
	 */
	@Override
	public void update(Observable o, Object arg) {
		if( o instanceof FactoryWorker){ //weryfikujemy ze podany observable jest odpowiedniego typu
			FactoryWorker worker = (FactoryWorker)o ; // rzutujemy obiekt do poprawnego typu
			
			// wyciągamy dodatkowe informacje z obiektu
			System.out.println(worker.getName() + " skończył pracę i zwrócił " + arg + " produktów.");
		}
	}
}
